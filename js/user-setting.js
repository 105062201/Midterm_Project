
function init() {
    var user_email = '';
    var btnchangename = document.getElementById('btnchangename');
    var changename = document.getElementById('inputname');
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var id = document.getElementById('userid');
        var name = document.getElementById('username');
        var email = document.getElementById('useremail');
        var gender = document.getElementById('usergender');
        var birthday = document.getElementById('userbirthday');
        if (user) {
            id.innerHTML = "<strong>個人ID: </strong>NULL " + "<br>";
            name.innerHTML = "<strong>名字name: </strong>NULL "+"<br>";
            email.innerHTML = "<strong>信箱email: </strong>NULL" + "<br>";
            gender.innerHTML = "<strong>性別gender: </strong>NULL" + "<br>";
            birthday.innerHTML = "<strong>生日birth: </strong>NULL"+ "<br>";
            user_email = user.email;
            var user_uid = user.uid;
            var postsRef = firebase.database().ref('user_list/' + user_uid);
            /*postsRef.once('value', function(snapshot) {
                var childData = snapshot.val();
                id.innerHTML = "個人ID: " + childData.userId + "<br>";
                name.innerHTML = "名字name: " + childData.username + "<br>";
                email.innerHTML = "信箱email: " + childData.useremail + "<br>";
                gender.innerHTML = "性別gender: " + childData.usergender + "<br>";
                postcnt.innerHTML = "發文數: " + childData.userpostcnt + "<br>";
              });*/
            postsRef.once("value")
                .then(function(snapshot) {
                    snapshot.forEach(function (childSnapshot){
                        var childData = childSnapshot.val();
                        //window.alert("getdata");
                        id.innerHTML = "<strong>個人ID: </strong>" + childData.userID + "<br>";
                        name.innerHTML = "<strong>名字name: </strong>" + childData.name + "<br>";
                        email.innerHTML = "<strong>信箱email:</strong> " + childData.email + "<br>";
                        gender.innerHTML = "<strong>性別gender: </strong>" + childData.gender + "<br>";
                        birthday.innerHTML = "<strong>生日birth: </strong>" + childData.birthday + "<br>";
                    });
                });
           menu.innerHTML = "<span class='dropdown-item' id='user-btn'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            var user_button = document.getElementById('user-btn');
            user_button.addEventListener('click', function () {
                document.getElementById("mySidenav").style.width = "250px";
                document.getElementById("main").style.marginLeft = "250px";
            });
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!');
                        window.location = "index.html";
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            

/*
            btnchangename.addEventListener('click', function () {
                var newname = changename.value;
                var newpostref = firebase.database().ref('user_list/'+user_uid);
                newpostref.update({
                        username:newname
                    });
                //window.location = "user-setting.html";
            });*/
            /*
            btnchangename.addEventListener('click', function () {
                var newname = changename.value;
                var newpostref = firebase.database().ref('user_list/'+user_uid);
                newpostref.once('value',function(snapshot){
                    snapshot.forEach(function(childSnapshot){
                        firebase.database().ref('user_list/'+user_uid + '/' +childSnapshot.key).update({
                            username:newname
                        });
                    });
                });
                //window.location = "user-setting.html";
            });*/
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }

        var storageRef = firebase.storage().ref();
        //var user = firebase.auth().currentUser;
        storageRef.child(user.uid).getDownloadURL().then(function (url) {
            var xhr = new XMLHttpRequest();
            xhr.responseType = 'blob';
            xhr.onload = function(event) {
                var blob = xhr.response;
            };
            xhr.open('GET', url);
            xhr.send();
            var img = document.createElement('img');
            img.src = url;
            img.style.right = 200;
            img.style.top = 100;
            img.width = 300;
            img.height = 300;
            document.getElementById("userpic").appendChild(img);
        }).catch(function (error) {
            document.getElementById("userpic").innerHTML = "<div style='color:red;'>您還未上傳個人照片，請至setting中上傳</div>"
            console.log(error.message);
        });
    });
    
    
        //document.getElementById("username").innerHTML = Id;
}



window.onload = function () {
    init();
}

$(window).on('load',function() {
    $('.flexslider').flexslider({
        animation: "slide"
      });
  });

/* Set the width of the side navigation to 0 */
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0px";
} 

function getUserId(email){
    var pos = email.indexOf("@");
    var UserId = email.slice(0,pos);
    return UserId;
}
/*
    var storageRef = firebase.storage().ref();
    //var user = firebase.auth().currentUser;
    storageRef.child(user.uid).getDownloadURL().then(function (url) {
        var xhr = new XMLHttpRequest();
        xhr.responseType = 'blob';
        xhr.onload = function(event) {
            var blob = xhr.response;
        };
        xhr.open('GET', url);
        xhr.send();
        var img = document.createElement('img');
        img.src = url;
        img.style.right = 200;
        img.style.top = 100;
        img.width = 70;
        img.height = 70;
        document.getElementById("userpic").appendChild(img);
    }).catch(function (error) {
        document.getElementById("userpic").innerHTML = "哥哥還沒上傳圖喔"
        console.log(error.message);
    });*/
