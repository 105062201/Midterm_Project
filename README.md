# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Anime Forum]
* Key functions (add/delete)
    1. [Membership Mechanism]---可以signin by email/signin by google account/signout
    2. [Database read/write]  
    User page:具備個人資料頁面(information)，有個人資料與個人照片(初始皆為NULL)，可透過(setting)來更新個人資料與上傳個人照片  
    Post page:可於每部的動畫的下方看到他人的評論，並留下你的評論  
    Post list page/Leave comment under any post:可以針對每一則貼文評論，另開一個頁面，於其中針對該篇評論進行回文，貼文皆為realtime的操作
    3. [RWD]---使用bootstrap的網格系統，透過他的響應式斷點便可完成RWD
    4. [Host on firebase page]
* Other functions (add/delete)
    1. [Third-Party Sign In]--you can use google to signin
    2. [Chrome Notification]--當你停留在某動畫的頁面時，每當有新貼文，只要有新的評論，便會跳出notification於螢幕右下角，點擊該Notification便可到該動畫評論頁面
    3. [Use CSS Animation]--網頁背景使用animation，可不停動態變色
    4. [Use firebase storage]---可上傳自己的頭像至資料庫，需要顯示時再從資料庫載回來
    5. [Use sidebar]----可以由點選my account--->email address，觸發sidebar的滑動效果，進而點選你的setting或是information來進行下一步操作
    6. [Use image slider]-----主頁有可以自動橫向滑動的slider，也可手動滑動，觀看目前可評論動畫
    7. [Use gallery]----每一部動畫，點擊該動畫圖片可放大跳出屬於該圖片的頁面，點擊下方名稱即可進入評論的頁面(注意:要點擊動畫標題才可進入該篇的討論，非點擊圖片)

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
這是一個動畫論壇網站Anime Forum，提供給大家討論該季新番，或是評論該新番。目前有三部提供討論，上方的genere僅為架構，皆設為重新連回Home page

## Security Report (Optional)
* database的rule read跟write都設為使用者不可隨意更動
* 每次進入頁面時，都會在onAuthStateChanged中判斷是否登入，如果未登入則無法查看任何貼文與上傳貼文，並且會跳出提醒alert告知請登入
